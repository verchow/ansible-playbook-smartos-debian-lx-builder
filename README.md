# Debian LX-brand Images for SmartOS 

## TL;DR
1. build a KVM on your SmartOS host
1. Install your desired Debian into that KVM
1. debootstrap Debian into foldername.tar.gz
1. convert foldername.tar.gz into an lx-brand image on your SmartOS-Host

## Prerequirement
- ISO file for booting and installing Debian
- possibility to install Debian into an empty KVM (i.e. via vnc)
- an `id_rsa.pub` in `.ssh` on your ansible host (because root login withou key is disabled by default)
- ... maybe something is missing

## Process
1. adopt `vars.yml` and `ansible.cfg` to your needs
1. run playbook `create_kvm.yml`: build a KVM and boot with your install ISO
  * the KVM's name is "lx-builder" (ensure DNS queries leads to this host)
  * for more hardware options have a look into the playbook
  * host and port for a vnc client will be given at the end of the play
1. perform a minimal installation with the installer medium
  * create a user and remember the password! (later remote_user password)
  * remember the root password! (later become password)
1. run playbook `create_tarball.yml`: build a tarball with debian-lx-brand-image-builder script
  * run `ansible-playbook --ask-pass --ask-become-pass create_tarball.yml` 
  * that makes a debootstrap debian on the KVM into `/root/lx`
  * all results are located in `/root/debian-lx-brand-image-builder` and copied to `/tmp` to make it accessable to everyone
1. copy the folder `/tmp/debian-lx-brand-image-builder` on a SmartOS host
1. run playbook `create_lx_image.yml`: build an lx-brand image (.zfs.gz) und corresponding manifest (.json)
  * has to be run on the SmartOS host!
  * create the image in `/tmp`, i.e. it has to be copied to somewhere else
1. Import your lx-brand image with `imgadm install -m MANIFEST.json -f ZONEFILE.zfs.gz`
1. check with `imgadm list`

## Links
* [debian-lx-brand-imgage-builder](https://github.com/joyent/debian-lx-brand-image-builder) on github
